﻿using System.Web;
using System.Web.Mvc;

namespace AppService.L200.Perf._2
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
