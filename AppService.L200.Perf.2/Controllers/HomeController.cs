﻿using AppService.L200.Perf._2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AppService.L200.Perf._2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult SomeError()
        {
            Crash();
            return View();
        }
        public void Crash()
        {
            var th = new System.Threading.Thread(ExecuteInForeground);
            th.Start();
            System.Threading.Thread.Sleep(5 * 1000);
        }
        private static void ExecuteInForeground()
        {
            throw new Exception("this is second chance exception");
        }

        void ErrorOne()
        {
            for (int i = 0; i < 10000; i++)// System.Threading.Thread.Sleep(1);
                ErrorOne();
        }
        static int count = 0;
        public ActionResult PerfError()
        {
            var tasks = new List<Task>();

            ViewBag.StringLength = 0;
            StringBuilder sb = new StringBuilder();
            int j = 0;
            try
            {
                for (int i = 0; i < 30; i++)
                {
                    Task t1 = Task.Run(() =>
                    {
                        Mysocket m = new Mysocket();
                        sb.Append(m.StartClient(i));
                       // System.Threading.Thread.Sleep(1);
                        j++;
                    });
                    tasks.Add(t1);
                }
                bool done = false;
                do
                {
                    var t = Task.WhenAny(tasks);
                    if(t == null)
                    {
                        done = true;
                    }
                    else if (t.Exception != null)
                    {
                        ViewBag.ErrorMessage = t.Exception.Message;
                        done = true;
                    }
                } while (!done);
            }
            catch(Exception e)
            {   
                var exceptions = tasks.Where(t => t.Exception != null).Select(t => t.Exception);

                ViewBag.ErrorMessage = exceptions.First().Message;
            }
            ViewBag.J = "J = " + j;
            ViewBag.Text = sb.ToString();

            ViewBag.StringLength = $"List Count ={leak.Count()} count = {count}";
            //int length = 0;
            //List<WebClient> l = new List<WebClient>();
            //try
            //{
            //    for(int i=0;i<300;i++)
            //    {
            //        WebClient x = new WebClient();
            //        var s = x.DownloadString("http://google.com");
            //        length += s.Length;
            //        l.Add(x);
            //    }
            //}
            //catch(Exception e)
            //{
            //    ViewBag.ErrorMessage = e.Message;
            //}
            //ViewBag.StringLength = length

            return View();
        }

        static List<Socket> leak = new List<Socket>();

        

            public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}